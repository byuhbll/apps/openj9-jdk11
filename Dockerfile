FROM registry.access.redhat.com/ubi7/ubi

ARG J9_URI=https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.4%2B11_openj9-0.15.1/OpenJDK11U-jdk_x64_linux_openj9_11.0.4_11_openj9-0.15.1.tar.gz

ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8

RUN cd /opt && \
  curl -L -o java.tar.gz $J9_URI && \
  mkdir java && \
  tar -xzvf java.tar.gz --strip-components=1 -C java && \
  chmod 755 java -R && \
  rm -f java.tar.gz && \
  cd /usr/bin && \
  ln -s /opt/java/bin/java java && \
  ln -s /opt/java/bin/javac javac && \
  mkdir /javasharedresources && \
  chmod 777 /javasharedresources && \
  chmod 777 /srv

RUN cd /usr/bin && \
  curl -o alert https://bitbucket.org/byuhbll/alert-cli/raw/1.0.0/alert && \
  chmod a+x alert

EXPOSE 8080 8081

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]