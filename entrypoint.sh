#! /bin/bash

cd /srv

java \
  -server \
  -XX:+IdleTuningGcOnIdle \
  -XX:+IdleTuningCompactOnIdle \
  -XX:IdleTuningMinIdleWaitTime=30 \
  -Xtune:virtualized \
  -Xshareclasses \
  -XX:sharedCacheHardLimit=200m \
  -Xscmx16m \
  -Xshareclasses:name=hbll \
  -Dspring.config.additional-location=/srv/config/ \
  -jar app.jar $@